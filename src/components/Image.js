import React, { Component } from 'react';

class Image extends Component {
    constructor(props) {
        super(props);
        this.state = { visible: false };
        this.handleLoad = this.handleLoad.bind(this);
    }

    handleLoad() {
        this.setState({ visible: true });
    }

    render() {
        return (
            <img
                className={["Image", this.state.visible ? "show" : "hide"].join(" ")}
                src={this.props.url}
                alt={this.props.altText}
                onLoad={this.handleLoad}
            />
        );
    }
}

export default Image;
