import React, { Component } from 'react';

class NavButton extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const increment = this.props.direction === "forward" ? 1 : -1;
        this.props.onNavigate(increment);
    }

    render() {
        return (
            <button onClick={this.handleClick}>{this.props.direction}</button>
        );
    }
}

export default NavButton;
