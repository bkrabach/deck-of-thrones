import React, { Component } from 'react';
import Card from './Card';
import '../css/CardList.css';

class CardList extends Component {
    render() {
        const startIndex = (this.props.page) * this.props.pageSize;
        const endIndex = startIndex + this.props.pageSize;

        return (
            <div className="CardList">
                {this.props.cards.slice(startIndex, endIndex).map((card) =>
                    <Card key={card.octgn_id} card={card} />
                )}
            </div>
        );
    }
}

export default CardList;
