import React, { Component } from 'react';
import NavButton from './NavButton';

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.handleNavigate = this.handleNavigate.bind(this);
    }

    handleNavigate(value) {
        this.props.onNavigate(value);
    }

    render() {
        return (
            <div className="navigation">
                <NavButton direction="back" onNavigate={this.handleNavigate} />
                <NavButton direction="forward" onNavigate={this.handleNavigate} />
            </div>
        );
    }
}

export default Navigation;
