import React, { Component } from 'react';
import Image from './Image';
import '../css/Card.css';

class Card extends Component {
    render() {
        return (
            <div className={["Card", this.props.card.type_code].join(" ")} >
                <Image url={this.props.card.image_url} altText={this.props.card.label} />
            </div>
        );
    }
}

export default Card;
