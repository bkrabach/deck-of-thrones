import React, { Component } from 'react';
import Navigation from './Navigation';
import CardList from './CardList';
import '../css/App.css';

class App extends Component {
  constructor(props) {
      super(props);
      this.state = {
          page: 0
      };
      this.handleNavigate = this.handleNavigate.bind(this);
  }

  handleNavigate(increment) {
      this.setState((prevState) => ({ page: this.getNewPageNumber(prevState.page, increment) }));
  }

  getNewPageNumber(existingPage, increment) {
      const maxPages = Math.round(this.props.cards.length / this.props.pageSize) - 1;
      return Math.min(Math.max(existingPage + increment, 0), maxPages);
  }

  render() {
      return (
          <div className="app">
              <Navigation onNavigate={this.handleNavigate} />
              <CardList cards={this.props.cards} page={this.state.page} pageSize={this.props.pageSize} />
          </div>
      );
  }
}

export default App;
