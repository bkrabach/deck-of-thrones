import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import cards from './data/thrones-db';
import App from './components/App';
import registerServiceWorker from './helpers/registerServiceWorker';

// Remove cards w/o images
const filteredCards = cards.filter(card => (card.image_url));
const pageSize = 21;

ReactDOM.render(<App cards={filteredCards} pageSize={pageSize} />, document.getElementById('root'));
registerServiceWorker();
